package com.example.iproyecto;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    //Registro de usuario
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<DatosRegistro> datosRegistro(@Field("username") String correo, @Field("password") String contrasena);

    //Login de usuario
    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<DatosLogin> datosLogin(@Field("username") String nombreUsuario,@Field("password") String contrasena);

    //Guardar Comentario
    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Menu> guardarComentario(@Field("username") String usuarioId,@Field("titulo") String titulo,@Field("descripcion") String descripcion);


    //Obtener todos los Usuarios
    @FormUrlEncoded
    @POST("api/todosUsuarios")
    Call<DatosListaUsuarios> todosUsuarios();

    /*
    //Detalles de Usuarios
    @FormUrlEncoded
    @POST("api/detallesUsuario")
    Call<> detallesUsuarios(@Fiel()

    @GET("api/todasNot")
    Call<Peticion_Login> getNoticias();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Menu> guardarNotificacion(@Field("usuarioId") String usuarioId,@Field("titulo") String titulo,@Field("descripcion") String descripcion);
    */

}
