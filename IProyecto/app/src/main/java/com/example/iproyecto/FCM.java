package com.example.iproyecto;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;

public class FCM extends FirebaseMessagingService{
    //guardar el token unico de aplicacion
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("token","mi token es: " + s);
        guardarTokenNuevo(s);
    }

    //recibe el token y hace referencia a la base de datos
    public void guardarTokenNuevo(String s)
    {
        DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("token");
        ref.child("usuarioId").setValue(s);

    }
}





