package com.example.iproyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public String APItoken;
    public String NombreUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        SharedPreferences preferences= getSharedPreferences("credenciales",Context.MODE_PRIVATE);
        String token=preferences.getString("Token","");
        if (token != "")
        {
            Intent exitToken=new Intent(MainActivity.this,Menu.class);
            startActivity(exitToken);
        }


        final Button btnInicioSecion=(Button) findViewById(R.id.buttonLogin);
        btnInicioSecion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText etUsuario=(EditText) findViewById(R.id.editTextUsuario);
                EditText etContrasena=(EditText) findViewById(R.id.editTextContrasena);
                String usuario=etUsuario.getText().toString();
                String contrasena=etContrasena.getText().toString();

                if (TextUtils.isEmpty(usuario))
                {
                    etUsuario.setError("Usuario no Ingresado");
                    etUsuario.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(contrasena))
                {
                    etContrasena.setError("Contraseña no Ingresada");
                    etContrasena.requestFocus();
                    return;
                }

                ServicioPeticion service =API.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<DatosLogin> datosLoginCall=service.datosLogin(etUsuario.getText().toString(),etContrasena.getText().toString());
                datosLoginCall.enqueue(new Callback<DatosLogin>() {
                    @Override
                    public void onResponse(Call<DatosLogin> call, Response<DatosLogin> response) {
                        DatosLogin login=response.body();

                        if (response.body()==null)
                        {
                            Toast.makeText(MainActivity.this,"Hubo un Error",Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (login.estado=="true")
                        {
                            APItoken=login.token;
                            NombreUsuarios=login.usuario;

                            GuardarDescripcion();
                            Toast.makeText(MainActivity.this,"Seccion Iniciada, BIENVENIDO : " + NombreUsuarios,Toast.LENGTH_LONG).show();
                            Intent IraPanel = new Intent(MainActivity.this,Menu.class);
                            IraPanel.putExtra("username",NombreUsuarios);
                            startActivity(IraPanel);

                        }
                        else
                        {
                            Toast.makeText(MainActivity.this,login.datalles,Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DatosLogin> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Ocurrio Un Error ",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

    }
    public void IrRegristros(View view)
    {
        Intent Registro =new Intent(MainActivity.this,Registro.class);
        startActivity(Registro);
    }




    public void GuardarDescripcion()
    {
        SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token=APItoken;
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putString("Token :",token);
        editor.commit();
    }




}


